class RestaurantesController < ApplicationController
    before_action :set_restaurante, only: [:edit, :update, :show, :destroy]
    def index
        @restaurantes = Restaurante.order :nome
    end

    def show
    end

    def destroy
        @restaurante = Restaurante.find(params[:id])
        @restaurante.destroy
        redirect_to(action: "index")
    end

    def new
        @restaurante = Restaurante.new
    end

    def edit
    end

    def update
        if@restaurante.update(restaurante_params)
            flash[:notice] = "Restaurante foi criado com sucesso"
            redirect_to restaurantes_path
        else
        render 'edit'
        end
    end
    

    def create
        @restaurante = Restaurante.new restaurante_params
        if @restaurante.save
            flash[:notice] = "Restaurante foi criado com sucesso"
            redirect_to restaurantes_path
        else 
            render 'new'
        end 
    end
    
    private 
    def set_restaurante
        @restaurante = Restaurante.find(params[:id])
    end

    def restaurante_params
        params.require(:restaurante).permit(:nome, :endereco, :especialidade)
    end
        
end
