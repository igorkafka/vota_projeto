class AddClienteAndRestauranteToQualificacao < ActiveRecord::Migration[5.2]
  def change
    def change
      add_index(:qualificacoes, :cliente_id)
      add_index(:qualificacoes, :restaurante_id)
      end      
  end
end
