class AddIndexToComentarios < ActiveRecord::Migration[5.2]
  def change
    add_index :comentarios, :comentavel_type
    add_index :comentarios, :comentavel_id
  end
end
